FROM amazoncorretto:21-alpine
LABEL org.opencontainers.image.authors="fobo66@protonmail.com"

ENV VERSION_TOOLS="11076708"

ENV ANDROID_SDK_ROOT "/sdk"
# Keep alias for compatibility
ENV ANDROID_HOME "${ANDROID_SDK_ROOT}"
ENV PATH "$PATH:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${ANDROID_SDK_ROOT}/platform-tools"

RUN apk update \
 && apk --no-cache add \
      ruby \
      curl \
      git \
      unzip \
      gcompat \
      libgcc


RUN curl -s https://dl.google.com/android/repository/commandlinetools-linux-${VERSION_TOOLS}_latest.zip > /cmdline-tools.zip
RUN mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools
RUN unzip /cmdline-tools.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools
RUN mv ${ANDROID_SDK_ROOT}/cmdline-tools/cmdline-tools ${ANDROID_SDK_ROOT}/cmdline-tools/latest
RUN rm -v /cmdline-tools.zip

RUN mkdir -p $ANDROID_SDK_ROOT/licenses/
RUN echo "824333f8a63b6825ea9c5514f83c2829b004d1fee" > $ANDROID_SDK_ROOT/licenses/android-sdk-license
RUN echo "84831b9409646a918e30573bab4c9c91346d8abd" > $ANDROID_SDK_ROOT/licenses/android-sdk-preview-license
RUN yes | sdkmanager --licenses >/dev/null

RUN mkdir -p /root/.android
RUN touch /root/.android/repositories.cfg
RUN sdkmanager --update

ADD packages.txt /sdk
RUN sdkmanager --package_file=/sdk/packages.txt
# For projects that may need Fastlane
RUN gem install bundler
